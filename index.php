<!DOCTYPE html>
<html lang="en">
<?php $page = "connection"?>
<?php include_once('part/head.html'); ?>
<?php include_once('part/nav.php') ?>
<?php
    $error = isset($_SESSION['errormessage']) ? $_SESSION['errormessage'] : '';
    $valid = isset($_SESSION['validmessage']) ? $_SESSION['validmessage'] : '';
    $mdperror = isset($_SESSION['mdpmessage']) ? $_SESSION['mdpmessage'] : '';

?>
<body>
<div class="container">
    <div class="login">
        <form method="POST" action="user/login_user.php" class="login_form">
                Login:
            <input type="text" name="login" class="form-control"  placeholder="Pseudo/login..." required>
                <br>
                Password:
            <input type="password" name="mdp"  class="form-control"  placeholder="Mot de passe..." required>
                <?php echo $error ?>
                <br>
                <br>
        <div class="button_class">
            <button type="submit" class="btn btn-primary" id="button1">Login</button>
            <button type="button" class="btn btn-secondary" onclick="register_screen()" id="button2">Register</button>
        </div>
        </form>
    </div>
    <div class="register">
        <form method="POST" action="user/create_user.php" class="login_form">
                New login:
            <input type="text" name="login_register" class="form-control" placeholder="Nouveau Username/Login..." required>
                <br>
                New password:
            <input type="password" name="mdp_register" class="form-control"  placeholder="Nouveau Mot de passe..." required>
                <br>
                Verifiy password:
            <input type="password" name="mdp_verify_register" class="form-control" placeholder="Revérifiez votre Mot de passe..."required>
                <?php echo $valid ?>
                <?php echo $mdperror ?>
                <br>
                <br>
        <div class="button_class">
    <button type="submit" class="btn btn-primary" id="button3">Register</button>
    <button type="button" class="btn btn-secondary" onclick="login_screen()" id="button4">Login</button>
        </div>
    </div>
    </form>
</div>
    <?php session_destroy() ?>
    <?php include_once('part/footer.html') ?>
</body>
</html>