<?php

include("../config/config.php");

class DB {
    static function add($link,$user){
        $bdd_add = connect();
        $request = $bdd_add->prepare("INSERT INTO video VALUES(NULL,:text,:user)");
        $request->execute(['text'=>$link,'user'=>$user]);
        $result = $request->fetchAll();
    }

    static function delete($id){
        $bdd_delete = connect();
        $request = $bdd_delete->query("DELETE  FROM video WHERE id=$id");

    }

    static function login($login,$password){
        $bdd_login = connect();
        $request = $bdd_login->prepare("SELECT id FROM user WHERE login=:login AND password=:password");
        $request->execute(['login'=>$login,'password'=>$password]);
        $result = $request->fetchAll();
        return $result;
    }

    static function register($login,$password){
        $bdd_register = connect();
        $request = $bdd_register->prepare("INSERT INTO user VALUES(NULL,:login,:password)");
        $request->execute(['login'=>$login,'password'=>$password]);
        $result = $request->fetchAll();
    }
}

?>